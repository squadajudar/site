-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 04-Jul-2020 às 21:34
-- Versão do servidor: 5.7.30
-- versão do PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `gabrielh_cta`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `donates`
--

CREATE TABLE `donates` (
  `id_donate` int(11) NOT NULL,
  `name_donate` varchar(255) NOT NULL COMMENT 'Nome',
  `email_donate` varchar(255) NOT NULL COMMENT 'E-mail',
  `type_donate` char(2) NOT NULL COMMENT 'Tipo (PayPal = 1 / Deposito = 2)',
  `frequency_donate` int(2) NOT NULL COMMENT '1 - Unica \\ 2 Recorrente',
  `value_donate` varchar(255) NOT NULL COMMENT 'Valor',
  `data_donate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `donates`
--

INSERT INTO `donates` (`id_donate`, `name_donate`, `email_donate`, `type_donate`, `frequency_donate`, `value_donate`, `data_donate`) VALUES
(1, 'Gabriel', 'gabrielhenriquesp@gmail.com', '2', 1, '15.00', '2020-07-01 22:44:35'),
(2, 'Gabriel', 'gabrielhenriquesp@gmail.com', '1', 1, '45.50', '2020-07-01 22:45:19'),
(3, 'Doação Depósito', 'gabrielprado.4505@aluno.una.br', '2', 1, '15.00', '2020-07-03 00:31:20'),
(4, 'Doação PayPal Unica', 'gabrielprado.4505@aluno.una.br', '1', 2, '15.00', '2020-07-03 00:31:54'),
(5, 'Doação PayPal Unica', 'gabrielprado.4505@aluno.una.br', '1', 2, '15.00', '2020-07-03 00:32:44'),
(6, 'Doação PayPal Recorrente', 'gabrielprado.4505@aluno.una.br', '1', 2, '15.00', '2020-07-03 00:33:20'),
(7, 'Doação PayPal Unica', 'gabrielprado.4505@aluno.una.br', '1', 1, '15.00', '2020-07-03 00:33:28'),
(8, 'Doação PayPal Unica', 'gabrielprado.4505@aluno.una.br', '1', 1, '30.00', '2020-07-03 01:49:15'),
(9, 'Doação Depósito', 'gabrielprado.4505@aluno.una.br', '2', 1, '89.90', '2020-07-03 01:49:44'),
(10, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '1', 2, '30.00', '2020-07-03 21:00:45'),
(11, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '1', 2, '30.00', '2020-07-03 21:01:22'),
(12, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '1', 2, '30.00', '2020-07-03 21:01:37'),
(13, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '1', 2, '30.00', '2020-07-03 21:01:59'),
(14, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '1', 2, '30.00', '2020-07-03 21:02:09'),
(15, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '1', 2, '30.00', '2020-07-03 21:02:21'),
(16, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '1', 2, '30.00', '2020-07-03 21:02:52'),
(17, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '1', 2, '30.00', '2020-07-03 21:03:16'),
(18, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '1', 2, '30.00', '2020-07-03 21:06:10'),
(19, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '1', 2, '30.00', '2020-07-03 21:06:31'),
(20, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '1', 2, '15.00', '2020-07-03 21:24:06'),
(21, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '1', 1, '15.00', '2020-07-03 22:23:05'),
(22, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '2', 1, '59.60', '2020-07-03 22:23:51'),
(23, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '1', 1, '15.00', '2020-07-03 22:58:22'),
(24, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '2', 1, '87.65', '2020-07-03 22:59:07'),
(25, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '1', 1, '15.00', '2020-07-04 15:37:58'),
(26, 'Gabriel Henrique', 'gabrielhenriquesp@gmail.com', '2', 1, '79.56', '2020-07-04 15:38:46');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ips`
--

CREATE TABLE `ips` (
  `id_ip` int(11) NOT NULL,
  `ip_ip` varchar(255) NOT NULL COMMENT 'IP',
  `email_ip` varchar(255) NOT NULL COMMENT 'E-mail Liberado',
  `data_ip` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data da Liberação'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `ips`
--

INSERT INTO `ips` (`id_ip`, `ip_ip`, `email_ip`, `data_ip`) VALUES
(7, '189.105.178.89', 'financas.bh@esf-brasil.org', '2020-07-01 22:55:08'),
(8, '191.185.110.77', 'davihely32@hotmail.com', '2020-07-02 10:44:44'),
(10, '187.20.38.33', 'gabrielprado.4505@aluno.una.br', '2020-07-03 00:36:44'),
(11, '179.179.248.161', 'franciscoafontoura@gmail.com', '2020-07-04 11:59:24'),
(12, '168.195.102.69', 'joaopedrocj@gmail.com', '2020-07-04 16:01:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `textos`
--

CREATE TABLE `textos` (
  `id_texto` int(11) NOT NULL,
  `dep_text` text NOT NULL COMMENT 'Texto Depósito',
  `alert_text` text NOT NULL COMMENT 'Alerta Pagamento Recorrente',
  `pag_text` varchar(255) NOT NULL COMMENT 'Valores Padrões'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `textos`
--

INSERT INTO `textos` (`id_texto`, `dep_text`, `alert_text`, `pag_text`) VALUES
(1, '<h5><strong>Transferência ou Depósito Bancário </strong></h5>\n<p><strong>Razão Social: </strong>Associação Engenheiros sem Fronteiras – Brasil<br /><strong>CNPJ: </strong>12.356.782/0001-85<br /><strong>Banco:</strong> 104 – Caixa Econômica Federal<br /><strong>Agência:</strong> 0584<br /><strong>Conta: </strong>35675-0<br /><strong>Operação:</strong> 013</p>', '<p>Todo mês, o PayPal irá debitar o valor informado nesse formulário de sua conta.</p>', '<p>15,00-30,00-50,00-98,00-145,50-91,40-11,50</p>');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `donates`
--
ALTER TABLE `donates`
  ADD PRIMARY KEY (`id_donate`);

--
-- Índices para tabela `ips`
--
ALTER TABLE `ips`
  ADD PRIMARY KEY (`id_ip`);

--
-- Índices para tabela `textos`
--
ALTER TABLE `textos`
  ADD PRIMARY KEY (`id_texto`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `donates`
--
ALTER TABLE `donates`
  MODIFY `id_donate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de tabela `ips`
--
ALTER TABLE `ips`
  MODIFY `id_ip` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de tabela `textos`
--
ALTER TABLE `textos`
  MODIFY `id_texto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
