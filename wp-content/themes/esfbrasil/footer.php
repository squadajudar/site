        <footer class="page-footer">
        	<div class="container">
        		<div>Copyright &copy; 2018 Engenheiros Sem Fronteiras</div>
        		<?php if (has_nav_menu('footer-menu')): ?>
					<div class="navbar navbar-expand-sm">
						<div class="clearfix">&nbsp;</div>
						<?php
			                echo wp_nav_menu(
			                    array(
			                        'echo'           => false,
			                        'menu_class'     => 'navbar-nav',
			                        'container'      => false,
			                        'walker'         => new Bootstrap_Nav_Walker(),
			                        'theme_location' => 'footer-menu',
			                    )
			                );
			            ?>
			            <div class="clearfix">&nbsp;</div>
			        </div>
		    	<?php endif; ?>
		    </div>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>
