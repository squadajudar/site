<?php
/*
Template Name: Margens largas
*/
?>

<?php get_header(); ?>

<div class="page">
	<div class="page-content">
    <?php
	    // Start the Loop.
	    while ( have_posts() ) : the_post();
	    	if (!has_post_thumbnail()) {
	            the_title('<h1 class="title">', '</h1>');
	        }
		    the_content();

		    // If comments are open or we have at least one comment, load up the comment template.
		    if ( comments_open() || get_comments_number() ) :
			    comments_template();
		    endif;
	    endwhile;
    ?>
	</div>
</div>

<?php get_footer(); ?>
