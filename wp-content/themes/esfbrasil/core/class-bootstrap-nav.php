<?php

class Bootstrap_Nav_Walker extends Walker_Nav_Menu {
    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        # FIXME: Y U NO OVERRIDE???
    }

    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        # FIXME: Y U NO OVERRIDE???
    }

    public function start_el( &$output, $object, $depth = 0, $args = array(), $current_object_id = 0 ) {
        $output .= '<a class="nav-item nav-link" href="' . $object->url . '">' . $object->title;
    }

    public function end_el( &$output, $object, $depth = 0, $args = array() ) {
        $output .= "</a>\n";
    }
}
