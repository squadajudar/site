<?php
function mapa_shortcode() {
   return <<<HTML
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <!--<h2 class="title text-left">onde estamos?</h2>-->
                <p class="text-left">No Brasil nossa organização possui mais de 70 núcleos, espalhados por todas as regiões do país.</p>

                <div class="d-none d-sm-block text-left" id="brasilmap-selecionado">
                    <h4 class="text-left">Clique no mapa para selecionar um estado</h4>
                </div>
            </div>


            <div class="col-md-6" style="padding: 2rem">
                <div class="d-none d-sm-block">
                    <img src="https://esf.org.br/wp-content/themes/esfbrasil/assets/img/onde-estamos/mapa.png" id="brasilmap-img" usemap="#brasilmap" class="img-fluid" hidefocus="true">
                </div>

                <div class="d-block d-sm-none text-center">
                    <img src="https://esf.org.br/wp-content/themes/esfbrasil/assets/img/onde-estamos/mapa.png" class="img-fluid">
                    <div>Selecione um estado</div>
                    <select id="brasilmap-select">
                        <option>--</option>
                        <option value="AC">Acre</option>
                        <option value="AL">Alagoas</option>
                        <option value="AP">Amapá</option>
                        <option value="AM">Amazonas</option>
                        <option value="BA">Bahia</option>
                        <option value="CE">Ceará</option>
                        <option value="DF">Distrito Federal</option>
                        <option value="ES">Espírito Santo</option>
                        <option value="GO">Goiás</option>
                        <option value="MA">Maranhão</option>
                        <option value="MT">Mato Grosso</option>
                        <option value="MS">Mato Grosso do Sul</option>
                        <option value="MG">Minas Gerais</option>
                        <option value="PA">Pará</option>
                        <option value="PB">Paraíba</option>
                        <option value="PR">Paraná</option>
                        <option value="PE">Pernambuco</option>
                        <option value="PI">Piauí</option>
                        <option value="RJ">Rio de Janeiro</option>
                        <option value="RN">Rio Grande do Norte</option>
                        <option value="RS">Rio Grande do Sul</option>
                        <option value="RO">Rondônia</option>
                        <option value="RR">Roraima</option>
                        <option value="SC">Santa Catarina</option>
                        <option value="SP">São Paulo</option>
                        <option value="SE">Sergipe</option>
                        <option value="TO">Tocantins</option>
                    </select>
                </div>
                <div class="d-block d-sm-none text-left" id="brasilmap-selecionado-celular"></div>

                <script type="text/javascript">
                    var nucleos;
                    $.getJSON("https://esf.org.br/wp-content/themes/esfbrasil/assets/data/nucleos.json", function( data ) {
                        console.log('Carregado lista de nucleos');
                        nucleos = data;
                    });

                    $(document).ready(function() {
                        if($('#brasilmap')) {
                            $('#brasilmap area').each(function() {
                                $(this).click(function() {
                                    siglaEstado = $(this).attr('alt');
                                    nomeEstado = $(this).attr('title');
                                    imgUrl = 'https://esf.org.br/wp-content/themes/esfbrasil'
                                    imgUrl += '/assets/img/onde-estamos/mapa-'+siglaEstado.toLowerCase()+'.png';
                                    $('#brasilmap-img').attr('src', imgUrl);

                                    var html = selecionaEstado(siglaEstado, nomeEstado);

                                    $('#brasilmap-selecionado').html(html);
                                });
                            });
                        }

                        if($('#brasilmap-select')) {
                            $('#brasilmap-select').change(function() {
                                siglaEstado = $('#brasilmap-select option:selected').val();
                                nomeEstado = $('#brasilmap-select option:selected').text();
                                var html = selecionaEstado(siglaEstado, nomeEstado);
                                $('#brasilmap-selecionado-celular').html(html);
                            });
                        }
                    });

                    function selecionaEstado(siglaEstado, nomeEstado) {
                        var html = '<h4 class="text-left">Núcleos '+nomeEstado+'</h4>';
                        num_nucleos = 0;
                        for (let nucleo in nucleos[siglaEstado]) {
                            num_nucleos++;

                            html += '- ';

                            if ('site' in nucleos[siglaEstado][nucleo] && nucleos[siglaEstado][nucleo]['site'] != '') {
                                html += '<a href="'+nucleos[siglaEstado][nucleo]['site']+'" target="_blank" rel="noopener noreferrer">'+nucleo+'</a>';
                            } else {
                                html += nucleo;
                            }

                            if ('facebook' in nucleos[siglaEstado][nucleo] && nucleos[siglaEstado][nucleo]['facebook'] != '') {
                                html += ' <a href="'+nucleos[siglaEstado][nucleo]['facebook']+'" target="_blank" rel="noopener noreferrer"><i class="fab fa-facebook"></i></a>';
                            }

                            if ('instagram' in nucleos[siglaEstado][nucleo] && nucleos[siglaEstado][nucleo]['instagram'] != '') {
                                html += ' <a href="'+nucleos[siglaEstado][nucleo]['instagram']+'" target="_blank" rel="noopener noreferrer"><i class="fab fa-instagram"></i></a>';
                            }

                            if ('twitter' in nucleos[siglaEstado][nucleo] && nucleos[siglaEstado][nucleo]['twitter'] != '') {
                                html += ' <a href="'+nucleos[siglaEstado][nucleo]['twitter']+'" target="_blank" rel="noopener noreferrer"><i class="fab fa-twitter"></i></a>';
                            }

                            if ('linkedin' in nucleos[siglaEstado][nucleo] && nucleos[siglaEstado][nucleo]['linkedin'] != '') {
                                html += ' <a href="'+nucleos[siglaEstado][nucleo]['linkedin']+'" target="_blank" rel="noopener noreferrer"><i class="fab fa-linkedin"></i></a>';
                            }

                            html += '<br>';
                        }

                        if (num_nucleos == 0) {
                            html += 'Não existe nenhum núcleo neste estado. <a href="fundar-um-nucleo">Fundar um núcleo</a>.';
                        }
                        return html;
                    }

                    window.onload = function () {
                        var ImageMap = function (map) {
                                var n,
                                    areas = map.getElementsByTagName('area'),
                                    len = areas.length,
                                    coords = [],
                                    previousWidth = 500;
                                for (n = 0; n < len; n++) {
                                    coords[n] = areas[n].coords.split(',');
                                }
                                this.resize = function () {
                                    var n, m, clen,
                                        x = $("#brasilmap-img").width() / previousWidth;
                                    for (n = 0; n < len; n++) {
                                        clen = coords[n].length;
                                        for (m = 0; m < clen; m++) {
                                            coords[n][m] *= x;
                                        }
                                        areas[n].coords = coords[n].join(',');
                                    }
                                    previousWidth = $("#brasilmap-img").width();
                                    return true;
                                };
                                window.onresize = this.resize;
                            },
                            imageMap = new ImageMap(document.getElementById('brasilmap'));
                        imageMap.resize();
                    }
                </script>

                <map name="brasilmap" id="brasilmap">
                    <area alt="RS" title="Rio Grande do Sul" href="#RS" shape="poly" coords="262,496,303,445,281,421,244,416,213,455">
                    <area alt="SC" title="Santa Catarina" href="#SC" shape="poly" coords="306,444,284,421,253,412,255,402,315,398,319,424">
                    <area alt="PR" title="Paraná" href="#PR" shape="poly" coords="318,399,255,398,249,369,271,352,304,366,327,392">
                    <area alt="SP" title="São Paulo" href="#SP" shape="poly" coords="329,392,304,364,273,351,291,319,311,327,334,328,344,363,361,356,360,371">
                    <area alt="RJ" title="Rio de Janeiro" href="#RJ" shape="poly" coords="367,372,367,355,399,339,411,354,397,372">
                    <area alt="ES" title="Espírito Santo" href="#ES" shape="poly" coords="415,354,397,335,412,303,431,308">
                    <area alt="MG" title="Minas Gerais" href="#MB" shape="poly" coords="362,354,395,338,417,302,416,291,425,282,375,257,346,261,332,303,306,303,294,316,338,324,346,360">
                    <area alt="DF" title="Distrito Federal" href="#DF" shape="poly" coords="316,257,321,281,340,279,340,262">
                    <area alt="GO" title="Goiás" href="#GO" shape="poly" coords="338,283,328,301,304,301,287,316,265,299,268,274,296,229,314,236,351,232,349,258,341,264,323,262,320,276,319,283">
                    <area alt="MS" title="Mato Grosso Do Sul" href="#MS" shape="poly" coords="238,374,277,347,287,318,262,299,240,292,217,290,206,302,204,350,229,352">
                    <area alt="BA" title="Bahia" href="#BA" shape="poly" coords="435,306,422,292,428,282,377,254,353,258,355,202,386,198,415,186,439,184,453,194,453,219,463,224,444,244">
                    <area alt="SE" title="Sergipe" href="#SE" shape="poly" coords="468,224,452,215,459,206,480,211">
                    <area alt="AL" title="Alagoas" href="#AL" shape="poly" coords="485,206,455,203,454,189,466,188,494,189">
                    <area alt="PE" title="Pernambuco" href="#PE" shape="poly" coords="499,185,420,184,429,166,496,173">
                    <area alt="PB" title="Paraíba" href="#PB" shape="poly" coords="498,173,451,168,449,152,496,156">
                    <area alt="RN" title="Rio Grande do Norte" href="#RN" shape="poly" coords="467,129,448,149,494,154,494,141">
                    <area alt="CE" title="Ceará" href="#CE" shape="poly" coords="416,109,427,164,448,170,447,156,463,133,445,113">
                    <area alt="PI" title="Piauí" href="#PI" shape="poly" coords="415,112,424,173,386,193,356,200,362,170,394,155,393,129,407,108">
                    <area alt="MA" title="Maranhão" href="#MA" shape="poly" coords="407,106,391,129,392,151,357,174,353,202,342,182,336,137,328,133,353,87">
                    <area alt="TO" title="Tocantins" href="#TO" shape="poly" coords="328,135,300,198,298,228,348,231,351,202,341,178">
                    <area alt="MT" title="Mato Grosso" href="#MT" shape="poly" coords="296,199,293,236,265,279,266,297,218,287,208,300,173,249,179,221,179,209,158,210,156,181,197,180,202,165,212,184">
                    <area alt="RO" title="Rondônia" href="#RO" shape="poly" coords="171,242,170,211,154,211,153,185,135,168,99,193,129,231">
                    <area alt="AC" title="Acre" href="#AC" shape="poly" coords="86,197,5,158,3,176,46,214,77,214">
                    <area alt="AM" title="Amazonas" href="#AM" shape="poly" coords="7,158,93,195,136,170,159,183,194,154,226,95,201,86,192,66,156,84,141,68,135,39,87,56,56,46,50,121,13,133">
                    <area alt="RR" title="Roraima" href="#RR" shape="poly" coords="118,16,138,40,146,58,143,68,156,78,189,66,195,53,174,41,174,5,146,22">
                    <area alt="PA" title="Pará" href="#PA" shape="poly" coords="194,66,228,92,198,154,214,182,298,192,324,142,351,89,347,78">
                    <area alt="AP" title="Amapá" href="#AP" shape="poly" coords="298,64,285,18,269,41,198,50">
                </map>
            </div>
        </div>
    </div>
</div>
HTML;
}
?>
