<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

        <!--<link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>-->

        <?php wp_head(); ?>

        <!-- Bootstrap 4 -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,500,700,900|Roboto:300,400,500,700,900" rel="stylesheet"> 

        <!-- FontAwesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

        <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png" />

        <!-- Bootstrap 4 -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>
    <body>
        
        <?php  if(strpos(get_page_link(), 'revista')){ 
        $text = "text-transform: capitalize;";
        
        ?>
        
        <div
        <?php if (is_home()): ?>
           <div class="header banner" style="background-image: linear-gradient(#49335fe0, 100%, rgba(0, 0, 0, 0.5)100%), url(<?=header_image()?>)">
        <?php elseif (has_post_thumbnail()): ?> 
           <div class="header banner" style="background-image: linear-gradient(#49335fe0, 100%, rgba(0, 0, 0, 0.5)100%), url(<?=header_image()?>)">
        <?php endif; ?>
        <?php }  else { 
        $text = "text-transform: lowercase;";
        ?>
         <div
        <?php if (is_home()): ?>
            <div class="header banner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/tint.png), url(<?php header_image(); ?>);">
        <?php elseif (has_post_thumbnail()): ?>
            <div class="header banner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/tint.png), url(<?=get_the_post_thumbnail_url()?>);">
        <?php endif; ?>
        <?php } ?>
        
            <div class="container">

                <!-- Begin Navbar -->
                <div class="navbar navbar-header navbar-expand-md">
                    <div class="navbar-brand">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
                            <span class="navbar-toggler-icon fa fa-bars"></span>
                        </button>

                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <img src="
                                <?php
                                    if (is_home() || has_post_thumbnail()) {
                                        echo get_theme_mod('logo_img_white', get_template_directory_uri().'/assets/img/logo-white.png');
                                    } else {
                                        echo get_theme_mod('logo_img', get_template_directory_uri().'/assets/img/logo.png');
                                    }
                                ?>
                            ">
                        </a>
                    </div>

                    <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'header-menu',
                                'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
                                'container'       => 'div',
                                'container_class' => 'collapse navbar-collapse',
                                'container_id'    => 'navbarNav',
                                'menu_class'      => 'navbar-nav ml-auto navbar-nav',
                                'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                                'walker'          => new WP_Bootstrap_Navwalker(),
                            )
                        );
                    ?>
                </div>
                <!-- End Navbar -->

                <?php  if (is_home()): ?>
                    <!-- Begin BannerContent -->
                    <div class="banner-content full-page">
                        <div class="offset-md-6 col-md-6 text-right">
                            <h1 style="font-size: 2rem;">
                                <?php
                                    echo get_theme_mod('banner_titulo', 'title');
                                ?>
                            </h1>
                            <div class="banner-subtitle">
                                <?php
                                    echo get_theme_mod('banner_subtitulo', 'subtitle');
                                ?>
                                </div>

                            <a
                                class="btn btn-transparent"
                                href="<?php echo esc_url( get_page_link( get_theme_mod('banner_cta_page', '1') ) ); ?>"
                            >
                                <?php echo get_theme_mod('banner_cta_text', 'subtitle'); ?>
                            </a>
                        </div>
                    </div>
                    <!-- End BannerContent -->
                <?php elseif (has_post_thumbnail()): ?>
                <!-- Begin BannerContent -->
                <div class="banner-content" style="margin: 20px; height: inherit !important;">
                  <?php the_title('<h1 class="title" style="'.$text.'">', '</h1>'); ?><br>
                </div>
                <div class="banner-content full-page" style="margin: 20px; height: inherit !important;">
                    <div class="col-md-12 text-right" style="margin-top: 10px; margin-bottom: 20px;">
                        
                    </div>
                </div>
                    <!-- End BannerContent -->
                <?php endif; ?>
            </>
          </div>
        </div>
