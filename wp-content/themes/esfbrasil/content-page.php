<div class="container">
    <div class="page-content">
        <?php
        if (!has_post_thumbnail()) {
            the_title('<h1 class="title">', '</h1>');
        }
        ?>
        <?php
        the_content();

        if (comments_open() || get_comments_number()) {
            comments_template();
        }
        ?>
    </div>
</div>
