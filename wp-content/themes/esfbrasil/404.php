<?php get_header(); ?>
<div class="page">
	<div class="container">
	    <div class="page-content">
	        <h1 class="title">Página não encontrada (404)</h1>
			<p>A página que você solicitou não existe</p>
		</div>
	</div>
</div>
<?php get_footer(); ?>
