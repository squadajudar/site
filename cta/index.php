<?php
include 'config.php';
include 'assets/header.php';

$text = (new \App\Text())->getRecurrencyText();
$values = (new \App\Text())->getValuesToSelect();
$vals = explode("-", $values->pag_text);
?>  
<nav class="navbar navbar-dark bg-dark" id="inicio">
    <div style="width: 18%;"></div>
    <span class="navbar-text">
        <h2 class="doacao navbar-brand"><?= $type ?></h2>
    </span>
    <?php if ($type == "PayPal") { ?>
        <h2 class="donate-2"><a class="navbar-brand color-cinza" href="<?= BASE ?>?donate=dep">doação por depósito</a></h2>
    <?php } else if ($type == "Depósito") { ?>
        <h2 class="donate-2"><a class="navbar-brand color-cinza" href="<?= BASE ?>">doação por PayPal</a></h2>
    <?php } ?>
</nav>
<br>
<div class="container">
    <form name="doacao" id="doacao" action="process" novalidate="novalidate" method="post">
        <div class="control-group">
            <div class="control-group" >
                Valor da Doação:
                <select class="form-control" id="select_val" name="select_val">
                    <?php foreach ($vals as $val) { ?>
                        <option value="<?= strip_tags(str_replace([".", ","], ["", "."], $val)) ?>">R$ <?= $val ?></option>
                    <?php } ?>
                    <option value="-" >Outro Valor</option>
                </select>
            </div>
        </div>
        <?php
        if ($type == "Depósito") {
            echo '<input class = "form-control" type = "hidden" name="select_type_donate" id="select_type_donate" required value = "1">';
        }
        if ($type != "Depósito") {
            ?>
            <?php if ($text->alert_text) { ?>
                <div class="control-group show-recurrency alert alert-info" style="width: 90%;display: none">
                    <?= $text->alert_text ?>
                </div>
            <?php } ?>
            <div class="control-group">
                <div class="control-group" >
                    Tipo da Doação:
                    <select class="form-control" id="select_type_donate" name="select_type_donate">
                        <option value="2">Doação Recorrente</option>
                        <option value="1">Doação Unica</option>
                    </select>
                </div>
            </div>

        <?php }
        ?>
        <div class = "control-group" id = "show-value" style = "display: none">
            <input class = "form-control" type = "text" name = "value" id = "value" placeholder = "Valor para Doação">
        </div>
        <div class = "control-group">
            <label>Nome:</label>
            <input class = "form-control" type = "text" name = "nome" id = "nome" required >
        </div>
        <div class = "control-group">
            <label>E-mail:</label>
            <input class = "form-control" type = "email" name = "email" id = "email" required >
        </div>
        <input class = "form-control" type = "hidden" name = "type" id = "type" required value = "<?= $type ?>">
        <br>
        <div id="success"></div>
        <div class="control-group">
            <div class="form-group btn-custom"><button class="btn btn-success" id="sendMessageButton" type="submit">prosseguir com a doação</button></div>
        </div>
    </form>
</div>

<?php include 'assets/footer.php'; ?>  

<script>
    $(".show-recurrency").show();
    $("#select_type_donate").change(function () {
        var type = $(this).val();
        if (type === '2') {
            $(".show-recurrency").show();
        } else {
            $(".show-recurrency").hide();
        }
    });
</script>