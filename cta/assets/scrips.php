<footer>
</footer>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/clean-blog.js"></script>
<script src="assets/js/validate.js"></script>
<script src="assets/js/scrips.js"></script>
<script src="assets/js/jquery.mask.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


<script>
    $("#select_val").change(function () {
        var opt = $(this).val();
        if (opt === '-') {
            $("#show-value").show();
            $("#value").attr("required", "req");
        } else {
            $("#show-value").hide();
            $("#value").removeAttr('required');
        }
    });
</script>
</body>
</html>