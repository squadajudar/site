<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="content-type" content="text/html; charset=utf-8">

        <link rel="pingback" href="http://gabrielhenriq.com.br/efs/xmlrpc.php">

        <!--<link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="http://gabrielhenriq.com.br/efs/xmlrpc.php" />
        -->

        <title>Apoie – EFS – BH</title>
        <link rel="dns-prefetch" href="//s.w.org">
        <script type="text/javascript">
            window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/", "ext":".png", "svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/", "svgExt":".svg", "source":{"concatemoji":"http:\/\/gabrielhenriq.com.br\/efs\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.2"}};
            /*! This file is auto-generated */
            !function(e, a, t){var r, n, o, i, p = a.createElement("canvas"), s = p.getContext && p.getContext("2d"); function c(e, t){var a = String.fromCharCode; s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, e), 0, 0); var r = p.toDataURL(); return s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, t), 0, 0), r === p.toDataURL()}function l(e){if (!s || !s.fillText)return!1; switch (s.textBaseline = "top", s.font = "600 32px Arial", e){case"flag":return!c([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) && (!c([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) && !c([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447])); case"emoji":return!c([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340])}return!1}function d(e){var t = a.createElement("script"); t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t)}for (i = Array("flag", "emoji"), t.supports = {everything:!0, everythingExceptFlag:!0}, o = 0; o < i.length; o++)t.supports[i[o]] = l(i[o]), t.supports.everything = t.supports.everything && t.supports[i[o]], "flag" !== i[o] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[i[o]]); t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t.readyCallback = function(){t.DOMReady = !0}, t.supports.everything || (n = function(){t.readyCallback()}, a.addEventListener?(a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1)):(e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function(){"complete" === a.readyState && t.readyCallback()})), (r = t.source || {}).concatemoji?d(r.concatemoji):r.wpemoji && r.twemoji && (d(r.twemoji), d(r.wpemoji)))}(window, document, window._wpemojiSettings);
        </script><script src="http://gabrielhenriq.com.br/efs/wp-includes/js/wp-emoji-release.min.js?ver=5.4.2" type="text/javascript" defer=""></script>
        <style type="text/css">
            img.wp-smiley,
            img.emoji {
                display: inline !important;
                border: none !important;
                box-shadow: none !important;
                height: 1em !important;
                width: 1em !important;
                margin: 0 .07em !important;
                vertical-align: -0.1em !important;
                background: none !important;
                padding: 0 !important;
            }
        </style>
        <link rel="stylesheet" id="wp-block-library-css" href="http://gabrielhenriq.com.br/efs/wp-includes/css/dist/block-library/style.min.css?ver=5.4.2" type="text/css" media="all">
        <link rel="stylesheet" id="contact-form-7-css" href="http://gabrielhenriq.com.br/efs/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.9" type="text/css" media="all">
        <script type="text/javascript" src="http://gabrielhenriq.com.br/efs/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp"></script>
        <script type="text/javascript" src="http://gabrielhenriq.com.br/efs/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1"></script>
        <script type="text/javascript" src="http://gabrielhenriq.com.br/efs/wp-content/plugins/flowpaper-lite-pdf-flipbook/assets/lity/lity.min.js"></script>
        <link rel="https://api.w.org/" href="http://gabrielhenriq.com.br/efs/wp-json/">
        <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://gabrielhenriq.com.br/efs/xmlrpc.php?rsd">
        <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://gabrielhenriq.com.br/efs/wp-includes/wlwmanifest.xml"> 
        <meta name="generator" content="WordPress 5.4.2">
        <link rel="canonical" href="http://gabrielhenriq.com.br/efs/apoie/">
        <link rel="shortlink" href="http://gabrielhenriq.com.br/efs/?p=56">
        <link rel="alternate" type="application/json+oembed" href="http://gabrielhenriq.com.br/efs/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fgabrielhenriq.com.br%2Fefs%2Fapoie%2F">
        <link rel="alternate" type="text/xml+oembed" href="http://gabrielhenriq.com.br/efs/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fgabrielhenriq.com.br%2Fefs%2Fapoie%2F&amp;format=xml">
        <style type="text/css" id="wp-custom-css">
            .qsImg {
                border-radius: 50%;
                filter: grayscale(100%);
                transition: filter 0.3s;
            }

            .qsImg:hover {
                filter: none;
            }

            .qsWrap {
                text-align: center; 
                justify-content: center;
            }

            .qsBlock {
                display: inline-block; 
                padding: 2rem;
            }

            .divMapa {
                width: 100%;
                margin-left: 10%;
            }

            #mapa {
                width: 100%;
            }		</style>

        <!-- Bootstrap 4 -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,500,700,900|Roboto:300,400,500,700,900" rel="stylesheet"> 

        <!-- FontAwesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

        <link rel="stylesheet" href="http://gabrielhenriq.com.br/efs/wp-content/themes/esfbrasil/style.css">
        <link rel="shortcut icon" href="http://gabrielhenriq.com.br/efs/wp-content/themes/esfbrasil/assets/img/favicon.png">

        <!-- Bootstrap 4 -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>Doações - Engenheiros sem fronteiras</title>
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic">
        <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/style.css">
        <link rel="shortcut icon" href="http://gabrielhenriq.com.br/efs/wp-content/themes/esfbrasil/assets/img/favicon.png" />
        <style>
            @import url('https://fonts.googleapis.com/css2?family=Lato&family=Roboto:wght@300&display=swap');
            #menu-item-59{

                background-color: white;
                border-radius: 5px;
            }
            #menu-item-59 .nav-link	{
                color: #459a69 !important;
            }
        </style>
    </head>
    <?php
    $fundo = "http://gabrielhenriq.com.br/efs/wp-content/themes/esfbrasil/assets/img/tint.png";
    $img = "http://gabrielhenriq.com.br/efs/wp-content/uploads/2020/05/integre-se-3.jpg";
    if (false) {
        $img = "assets/img/home-bg.png";
    }
    ?>
    <body>
        <header class="header banner" style="background-image:url(<?= $fundo ?>), url(<?= $img ?>);">
            <div class="overlay"></div>
            <div class="navbar navbar-header navbar-expand-md">
                <div class="navbar-brand">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
                        <span class="navbar-toggler-icon fa fa-bars"></span>
                    </button>

                    <a href="http://gabrielhenriq.com.br/efs/">
                        <img src="
                             http://gabrielhenriq.com.br/efs/wp-content/uploads/2020/05/b954e2bf-logo-branca.png                            ">
                    </a>
                </div>
                <div id="navbarNav" class="collapse navbar-collapse justify-content-end"><ul id="menu-principal" class="navbar-nav ml-auto navbar-nav"><li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-52" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52 nav-item"><a title="conheça a equipe" href="http://gabrielhenriq.com.br/efs/seja-um-voluntario/" class="nav-link">conheça a equipe</a></li>
                        <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-64" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64 nav-item"><a title="voluntariado" href="http://gabrielhenriq.com.br/efs/voluntariado/" class="nav-link">voluntariado</a></li>
                        <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-39" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-39 nav-item"><a title="Projetos" href="http://gabrielhenriq.com.br/efs/projetos/" class="nav-link">Projetos</a></li>
                        <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-77" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77 nav-item"><a title="transparência" href="http://gabrielhenriq.com.br/efs/transparencia/" class="nav-link">transparência</a></li>
                        <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-324" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-324 nav-item"><a title="ESFera" href="http://gabrielhenriq.com.br/efs/revista/" class="nav-link">ESFera</a></li>
                        <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-70" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-70 nav-item"><a title="Contato" href="http://gabrielhenriq.com.br/efs/contato/" class="nav-link">Contato</a></li>
                        <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-494" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-494 nav-item"><a title="Blog" href="http://gabrielhenriq.com.br/efs/blog/" class="nav-link">Blog</a></li>
                        <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-59" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59 nav-item"><a title="Apoie" href="http://gabrielhenriq.com.br/efs/apoie/" class="nav-link">Apoie</a></li>
                    </ul></div>           
            </div>
            <div class="container">
                <div class="banner-content full-page">
                    <div class="offset-md-12 col-md-12 ">
                        <h1 style="font-size: 2rem;">
                            JUNTOS SOMOS SEM FRONTEIRAS                        </h1>
                        <div class="banner-subtitle">
                            inspiramos pessoas e construímos o futuro.                             </div>

                    </div>
                </div>
            </div>
        </header>
        <?php include 'config.php'; ?>  