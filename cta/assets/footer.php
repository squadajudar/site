<footer>
</footer>
<div class="col-md-12 col-lg-12 ">
    <div class="row">
        <div class="col-md-12 col-lg-12 ">
            <ul class="list-inline text-center">
                <a href="https://www.facebook.com/EngenheirosSemFronteirasBH" target="blank" class="list-inline-item facebook"> 
                    <span class="fa-stack fa-lg"> 
                        <i class="fa fa-square fa-stack-2x"></i> 
                        <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                    </span>
                </a>
                <a href="https://www.instagram.com/esf.bh/" target="blank" class="list-inline-item insta"><span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-instagram fa-stack-1x fa-inverse"></i></span></a>
                <!--<a class="list-inline-item linkedin"><span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-linkedin fa-stack-1x fa-inverse"></i></span></a>-->
                <a href="https://www.youtube.com/channel/UCVKTPqcKZUuHwuMRYaGXBlQ" target="blank" class="list-inline-item youtube"><span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-youtube fa-stack-1x fa-inverse"></i></span></a>
            </ul>
            <p class="text-center text-muted copyright">Copyright&nbsp;©&nbsp;Engenheiros sem Fronteiras - BH <?= date('Y') ?></p>
        </div>
    </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/clean-blog.js"></script>
<script src="assets/js/validate.js"></script>
<script src="assets/js/scrips.js"></script>
<script src="assets/js/jquery.mask.js"></script>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


<script>
    $("#select_val").change(function () {
        var opt = $(this).val();
        if (opt === '-') {
            $("#show-value").show();
            $("#value").attr("required", "req");
        } else {
            $("#show-value").hide();
            $("#value").removeAttr('required');
        }
    });
</script>
</body>
</html>