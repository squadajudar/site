<?php

namespace App;

class Api
{

    private $apiUrl;
    private $apiKey;
    private $apiSecret;
    private $endPoint;
    private $build;
    private $callBack;

    public function __construct()
    {
        if (false) {
            $this->apiUrl = "https://api.sandbox.paypal.com/";
            $this->apiKey = "AVlPiwBhdQKWUOfxOo2Q63Y1DDc2UsypgkHx6Z_fwHE3SaeaKkBH0q9ETjFOxLbtAqr6qLGwSqcvgM7U";
            $this->apiSecret = "EDA73hQN3f8v8Ei30tkDQ4yQ0XuMp1sotafKKEQSndFh52w41kDUxuix3pOEZOVJ8ikauoEILJ0yml20";
            $this->productId = "PROD-3KJ28056NL6921453";
        } else {
            $this->apiUrl = "https://api.paypal.com/";
            $this->apiKey = "AbHhWPr9FB_HZxXd1jB9ghYcEht9kZbKnfg-sSr5E896SftY8CU8NiaAL6UKmXbeEsgaT302oh65RkPm";
            $this->apiSecret = "ELuynGTwPL2mY9NtSOiFs44THGX3r7IF5BYzq4xyrlHtlCkxMSo-ZVupcnP59oqAD7_f-W5la9v7dOMB";
            $this->productId = "PROD-82J83632KX6416410";
        }
    }

    public function getAccessToken()
    {
        $this->endPoint = "v1/oauth2/token";
        $url = $this->apiUrl . $this->endPoint;

        $data = http_build_query(array("grant_type" => "client_credentials"));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, "{$this->apiKey}:{$this->apiSecret}");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, []);

        $this->callBack = trim(curl_exec($ch));
        curl_close($ch);
        $this->callBack = (object) json_decode($this->callBack, true);

        return $this->callBack->access_token;
    }

    public function createProduct()
    {
        if (!empty($this->productId)) {
            return $this->productId;
        }
        $this->endPoint = "v1/catalogs/products";
        $url = $this->apiUrl . $this->endPoint;

        $post = array(
            "name" => "Doação recorrente - EFS BH",
            "description" => "Doação recorrente pelo site efs bh",
            "type" => "SERVICE",
            "category" => "SOFTWARE",
            "home_url" => BASE
        );
        $token = "Authorization: Bearer " . $this->getAccessToken();

        $data = json_encode($post);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $token));

        $this->callBack = trim(curl_exec($ch));
        curl_close($ch);
        $this->callBack = (object) json_decode($this->callBack, true);

        return $this->callBack;
    }

    public function createPlan($value)
    {
        $this->endPoint = "v1/billing/plans/";
        $url = $this->apiUrl . $this->endPoint;

        $post = '{
            "product_id":"' . $this->productId . '",
            "name":"Doação recorrente - EFS BH ' . date("Y-m-d") . '",
            "description":"Doação recorrente pelo site efs bh",
            "status":"ACTIVE",
                "billing_cycles":[
                    {
                        "frequency":{
                        "interval_unit":"MONTH",
                        "interval_count":1
                    },
            "tenure_type":"REGULAR",
            "sequence":1,
            "total_cycles":12,
            "pricing_scheme":{
                "fixed_price":{
                    "value":"'.$value.'",
                    "currency_code":"BRL"
                }
            }
      }
   ],
        "payment_preferences":{
        "auto_bill_outstanding":true,
        "setup_fee_failure_action":"CONTINUE",
        "payment_failure_threshold":3
        }
}';
        $token = "Authorization: Bearer " . $this->getAccessToken();

        $data = $post;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $token));

        $this->callBack = trim(curl_exec($ch));
        curl_close($ch);
        $this->callBack = (object) json_decode($this->callBack, true);

        return $this->callBack->id;
    }
    
}
