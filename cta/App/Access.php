<?php

namespace App;

use App\Model;

class Access extends Model
{

    public function checkIp(string $ip): bool
    {
        $ip = filter_var($ip, FILTER_SANITIZE_STRIPPED);

        $model = (new Model())->find("SELECT * FROM ips");
        $data = $model->fetch(true);

        if (empty($data)) {
            return false;
        }

        foreach ($data as $value) {
            if ($value->ip_ip == $ip) {
                return true;
            }
        }

        return false;
    }

    public function insertIp(string $email, string $ip): bool
    {
        $ip = filter_var($ip, FILTER_SANITIZE_STRIPPED);
        $email = filter_var($email, FILTER_VALIDATE_EMAIL);

        $insert = (new Model())->insertIp("INSERT INTO ips (`ip_ip`, `email_ip`) "
                . "VALUES (?, ?)", $ip, $email);

        return $insert;
    }

}
