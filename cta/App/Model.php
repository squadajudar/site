<?php

namespace App;

class Model
{

    private const OPTIONS = [
        \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_OBJ,
        \PDO::ATTR_CASE => \PDO::CASE_NATURAL
    ];

    private static $instance;

    public static function getInstance(): \PDO
    {
        if (empty(self::$instance)) {
            try {
                self::$instance = new \PDO(
                        "mysql:host=" . SERVER . ";dbname=" . BD_NAME,
                        BD_USER,
                        BD_PWD,
                        self::OPTIONS
                );
            } catch (\PDOException $exception) {
                var_dump("Erro database " . $exception);
                die;
            }
        }

        return self::$instance;
    }

    public function find($query)
    {
        $query = filter_var($query, FILTER_SANITIZE_STRIPPED);

        $this->query = $query;
        return $this;
    }

    public function insertIp($query, $ip, $email)
    {
        $query = filter_var($query, FILTER_SANITIZE_STRIPPED);
        $stmt = self::getInstance()->prepare($query);

        if ($stmt->execute([$ip, $email])) {
            return true;
        }
        return false;
    }

    public function insertDonate($query, $name, $email, $type, $value, $frequency)
    {
        $stmt = self::getInstance()->prepare($query);

        if ($stmt->execute([$name, $email, $type, $value, $frequency])) {
            return true;
        }
        return false;
    }

    public function updateTextDep($query, $txt)
    {
        $stmt = self::getInstance()->prepare($query);

        if ($stmt->execute([$txt])) {
            return true;
        }
        return false;
    }

    public function fetch(bool $all = false)
    {
        try {
            $stmt = self::getInstance()->prepare($this->query);
            $stmt->execute($this->params);

            if (!$stmt->rowCount()) {
                return null;
            }

            if ($all) {
                return $stmt->fetchAll(\PDO::FETCH_CLASS, static::class);
            }

            return $stmt->fetchObject(static::class);
        } catch (\PDOException $exception) {
            $this->fail = $exception;
            return null;
        }
    }

}
