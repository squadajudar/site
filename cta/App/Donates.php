<?php

namespace App;

use App\Model;

class Donates extends Model
{

    public function getAll(): ?array
    {
        $model = new Model();
        $model->find("SELECT * FROM donates ORDER BY data_donate DESC");
        return $model->fetch(true);
    }

    public function insertDonate(string $name, string $email, int $type, string $value, int $frequency): bool
    {
        $name = filter_var($name, FILTER_SANITIZE_STRIPPED);
        $value = filter_var($value, FILTER_SANITIZE_STRIPPED);
        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        $type = filter_var($type, FILTER_VALIDATE_INT);
        $frequency = filter_var($frequency, FILTER_VALIDATE_INT);

        $insert = (new Model())->insertDonate("INSERT INTO donates (name_donate,email_donate,type_donate,value_donate,frequency_donate) "
                . "VALUES (?, ?, ?, ?, ?)", $name, $email, $type, $value, $frequency);

        return $insert;
    }

}
