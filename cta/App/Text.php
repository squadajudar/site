<?php

namespace App;

use App\Model;

class Text extends Model
{

    public function getDepText(): ?object
    {
        $model = new Model();
        $model->find("SELECT dep_text FROM textos WHERE id_texto = 1");
        return $model->fetch();
    }

    public function getTextPag(): ?object
    {
        $model = new Model();
        $model->find("SELECT pag_text FROM textos WHERE id_texto = 1");
        return $model->fetch();
    }

    public function getRecurrencyText(): ?object
    {
        $model = new Model();
        $model->find("SELECT alert_text FROM textos WHERE id_texto = 1");
        return $model->fetch();
    }

    public function updateTextDeposito(string $text)
    {
        $insert = (new Model())->updateTextDep("UPDATE textos SET dep_text = ? "
                . "WHERE textos.id_texto = 1", $text);
        return $insert;
    }

    public function updateTextRec(string $text)
    {
        $insert = (new Model())->updateTextDep("UPDATE textos SET alert_text = ? "
                . "WHERE textos.id_texto = 1", $text);
        return $insert;
    }

    public function updateTextValues(string $text)
    {
        $insert = (new Model())->updateTextDep("UPDATE textos SET pag_text = ? "
                . "WHERE textos.id_texto = 1", $text);
        return $insert;
    }

    public function getValuesToSelect()
    {
        $model = new Model();
        $model->find("SELECT pag_text FROM textos WHERE id_texto = 1");
        return $model->fetch();
    }

}
