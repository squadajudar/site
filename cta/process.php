<?php

require 'config.php';

if (empty($_POST)) {
    $url = BASE;
    echo '<script> '
    . 'alert("Preencha o formulário antes de vir aqui ;) ");
        setTimeout(function () {
            window.location.replace("' . $url . '");
        }, 500);</script>';
    exit;
}
$data = $_POST;
$name = $data["nome"];
$value = str_replace([".", ","], ["", "."], $data["value"]);
if ($data['select_val'] != "-") {
    $value = str_replace([".", ","], ["", "."], $data["select_val"]);
}

require("source/class.phpmailer.php");

date_default_timezone_set('Brazil/East');
$email = new PHPMailer();
$email->IsHTML(true);
$email->Host = "mail.gabrielhenriq.com.br";
$email->SMTPAuth = true;
$email->SMTPSecure = "ssl";
$email->Port = 465;
$email->SetFrom('naoresponda@gabrielhenriq.com.br', 'CTA');
$email->Username = "naoresponda@gabrielhenriq.com.br";
$email->Password = "dO4!a.b~$!Sy";
$email->AddAddress("gabrielhenriquesp@gmail.com", "CTA");
$email->Subject = utf8_decode("CTA - Engenheiros sem fronteiras BH");
$email->MsgHTML(utf8_decode("Nova Doação:<br /><br />"
                . "Nome: " . $data['nome'] . " " . $data["sobrenome"] . "<br />"
                . "E-mail: " . $data['email'] . "<br />"
));
$value = ($data["select_val"] != "-" ? $data["select_val"] : $data["value"]);
$value = str_replace(',', '.', $value);

$time = date("d/m/Y H:i");

if (!$email->Send()) {
    var_dump("falha ao enviar o email");
}

insertDonate($data, $value, $time);

function insertDonate(array $data, string $value, string $time)
{
    $type = ($data["type"] == "PayPal" ? 1 : 2);
    $frequency = $data["select_type_donate"];

    $donate = (new \App\Donates())->insertDonate($data["nome"], $data["email"], $type, $value, $frequency);

    if (!$donate) {
        $email = new PHPMailer();
        $email->IsHTML(true);
        $email->Host = "mail.gabrielhenriq.com.br";
        $email->SMTPAuth = true;
        $email->SMTPSecure = "ssl";
        $email->Port = 465;
        $email->SetFrom('naoresponda@gabrielhenriq.com.br', 'CTA');
        $email->Username = "naoresponda@gabrielhenriq.com.br";
        $email->Password = "dO4!a.b~$!Sy";
        $email->AddAddress("gabrielhenriquesp@gmail.com", "CTA");
        $email->Subject = utf8_decode("Erro ao inserir doação - Engenheiros sem fronteiras BH");
        $email->MsgHTML(utf8_decode("Erro ao inserir Doação:<br /><br />"
                        . "Post: " . json_encode($data)
        ));
        $email->Send();
    }
}

include 'assets/header.php';


if ($data["type"] == "PayPal") {
    $description = "Doação Site EFS - BH | Doador: " . $data['nome'] . " | " . $data['email'];

    if ($data["select_type_donate"] == 1) {
        include 'paypal.php';
    } else if ($data["select_type_donate"] == 2) {
        $api = new App\Api();
        $plan = $api->createPlan($value);
        include 'paypal-subscription.php';
    }
} else if ($data["type"] == "Depósito") {
    $data["text"] = (new \App\Text())->getDepText();
    include 'deposito.php';
}
include 'assets/footer.php';
