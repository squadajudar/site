<div class="container">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-8 mx-auto deps-class">
                <div class="wp-block-columns">
                    <div class="wp-block-column">

                        <?= $data['text']->dep_text ?>

                        <b>Valor da Doação:</b> R$ <?= str_replace([",", "."], [".", ","], $value) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>