<?php

require 'config.php';

if (empty($_GET["email"])) {
    $url = BASE . "read";
    echo '<script> '
    . 'alert("Acesso Negado, favor tentar novamente, faltando e-mail");
        setTimeout(function () {
            window.location.replace("' . $url . '");
        }, 1000);</script>';
    exit;
}
if (empty($_GET["ip"])) {
    $url = BASE . "read";
    echo '<script> '
    . 'alert("Acesso Negado, favor tentar novamente, faltando ip!");
        setTimeout(function () {
            window.location.replace("' . $url . '");
        }, 1000);</script>';
    exit;
}

$email = filter_var($_GET["email"], FILTER_VALIDATE_EMAIL);
$ip = filter_var($_GET["ip"], FILTER_SANITIZE_STRIPPED);

$access = (new \App\Access())->insertIp($email, $ip);

if ($access) {
    include 'process-page/send-acept.php';
    $url = BASE . "read";
    echo '<script> '
    . 'alert("Acesso Liberado!");
        setTimeout(function () {
            window.location.replace("' . $url . '");
        }, 1000);</script>';
    exit;
}