<script src="https://www.paypal.com/sdk/js?client-id=<?= CLIENT_ID ?>&currency=BRL"></script>
<div class="container">
    <div class="col-md-12 col-lg-12 mx-auto deps-class" style="text-align: center">
        <div id="paypal-button-container"></div>
        <script>
            paypal.Buttons({
                createOrder: function (data, actions) {
                    return actions.order.create({
                        purchase_units: [{
                                amount: {
                                    value: '<?= $value ?>',
                                    currency: 'BRL',
                                    intent: 'capture'

                                },
                                description: '<?= $description ?>'
                            }]
                    });
                },
                onApprove: function (data, actions) {
                    return actions.order.capture().then(function (details) {
                        alert('Agradecemos sua doação!');
                        setTimeout(function () {
                            window.location.replace("http://gabrielhenriq.com.br/efs/apoie/");
                        }, 1000);
                    });
                }, onCancel: function (data) {
                    alert('Agradecemos sua doação!');
                    setTimeout(function () {
                        window.location.replace("http://gabrielhenriq.com.br/efs/apoie/");
                    }, 1000);
                }
            }).render('#paypal-button-container');

        </script>
    </div>
</div>