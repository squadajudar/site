<?php

require 'config.php';

$text = new \App\Text();
$update = $text->updateTextDeposito($_POST["content_deposito"]);

$updateRecurrency = $text->updateTextRec($_POST["content_recurrency"]);
$updateValues = $text->updateTextValues($_POST["content_pags"]);

$json["message"] = "<div class='message error icon-warning'>Erro ao alterar textos!</div>";

if ($update && $updateRecurrency) {
    $json["message"] = "<div class='message success icon-warning'>Texto alterado com sucesso!</div>";
}

echo json_encode($json);
exit;
