<div class="login">
    <article class="login_box radius">
        <h1 class="hl">Solicitar acesso</h1>
        <form name="acesso" id="acesso" action="read" novalidate="novalidate" method="post">
            <label>
                <span class="field ">E-mail:</span>
                <input type="email" name="email" id="email" placeholder="Informe seu e-mail" required="required">
                <input class="form-control" type="hidden" name="ip" id="ip" value="<?= $_SERVER["REMOTE_ADDR"] ?>" required > 
            </label>
            <button class="radius gradient gradient-green gradient-hover" >Solicitar</button>
        </form>
        <footer>
            <p>Desenvolvido por <a href="https://bh.esf.org.br/">bh.esf.org.br</a></p>
            <p>© <?= date('Y') ?> - todos os direitos reservados</p>
        </footer>
    </article>
</div>

<script>
    $("#acesso").validate({
        rules: {
            email: "required"
        }
    });
</script>