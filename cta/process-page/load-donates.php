<?php $donates = (new \App\Donates())->getAll(); ?>

<div class="padding">
    <div class="control-group">
        <div class="form-group btn-custom"><a href="textos" class="btn btn-success" type="submit">Gerênciar Textos</a></div>
    </div>
    <br>

    <h3 class="tittle">
        <a target="_blank">
            <span>Histórico de Doações:</span>
        </a>
    </h3>
</div>
<div class="padding">
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        table{

            overflow-x: scroll;
        }
        tbody {
            overflow-y: scroll;
            overflow-x: hidden;
            height: 140px;
        }
        th, td {
            padding: 15px;
        }th {
            position: sticky;
            top: 0;
            text-align: left;
        }table {
            border-spacing: 5px;
        }
    </style>
    <table  border="1" width="100%" id="tblNeedsScrolling">
        <thead>
        <th>Nome</th>
        <th>E-mail</th>
        <th>Tipo da Doação</th>
        <th>Periodicidade</th>
        <th>Valor</th>
        <th>Data</th>
        </thead>
        <?php
        foreach ($donates as $donate) {
            $type = ($donate->type_donate == "1" ? "PayPal" : "Depósito" );
            $frequency = ($donate->frequency_donate == "1" ? "Unica" : "Mensal" );
            echo '<tr>
        <td>' . $donate->name_donate . '</td>
        <td>' . $donate->email_donate . '</td>
        <td>' . $type . '</td>
        <td>' . $frequency . '</td>
        <td>R$ ' . str_replace([",", "."], [".", ","], $donate->value_donate) . '</td>
        <td>' . (new DateTime($donate->data_donate))->format("d/m/Y - H:i") . '</td>
    </tr>';
        }
        ?>
    </table>
</div>
<?php include 'assets/footer.php'; ?>  