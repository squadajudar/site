<?php
require 'config.php';

$ip = new App\Access();
$txt = new \App\Text();
$textDeposito = $txt->getDepText();
$textRecurrecy = $txt->getRecurrencyText();
$textPag = $txt->getTextPag();
include 'assets/scrips.php';
if (!$ip->checkIp($_SERVER["REMOTE_ADDR"])) {
    echo '<script> '
    . 'Swal.fire({ 
        icon: "error",
        title: "Oops...",
        text: "Acesso restrito, você será redirecionado!"
    });
        setTimeout(function () {
            window.location.replace("' . BASE . 'read");
        }, 3000);</script>';
    exit;
}
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Bem Vindo(a) Administrador | CTA </title> 
    <link rel="stylesheet" href="assets/shared/styles/boot.css">
    <link rel="stylesheet" href="assets/shared/styles/styles.css">
    <link rel="stylesheet" href="assets/admin/assets/css/style.css">

    <link rel="shortcut icon" href="http://gabrielhenriq.com.br/efs/wp-content/themes/esfbrasil/assets/img/favicon.png">

</head>
<body>
    <div class="ajax_response"></div>
    <div class="dash">
        <aside class="dash_sidebar">
            <article class="dash_sidebar_user">
                <div><img class="dash_sidebar_user_thumb" src="http://gabrielhenriq.com.br/efs/wp-content/themes/esfbrasil/assets/img/favicon.png" alt="" title=""></div>
                <h3 class="dash_sidebar_user_name">
                    <a href="<?= BASE ?>read">Administrador</a>
                </h3>
            </article>
        </aside>
        <section class="dash_content">
            <div class="dash_userbar">
                <div class="dash_userbar_box">
                    <div class="dash_content_box">
                        <h1 class="transition"> 
                            <a href="<?= BASE ?>read">CMS <b>Admin</b></a></h1>
                    </div>
                </div>
                <div class="notification_center"></div>
            </div>
            <form method="post" action="save-text">
                <br>
                <section class="dash_content_app">
                    <header class="dash_content_app_header">
                        <h2>Textos:</h2>
                    </header>
                    <div class="dash_content_app_box">
                        <section>
                            <div class="app_blog_home">
                                <article>
                                    <h3 class="tittle">
                                        <a target="_blank">
                                            <span>Dados do Depósito:</span>
                                        </a>
                                    </h3>
                                    <textarea class="mce" name="content_deposito"><?= $textDeposito->dep_text ?></textarea>
                                </article>
                            </div>
                    </div>

                </section>   
                <section class="dash_content_app">
                    <div class="dash_content_app_box">
                        <section>
                            <div class="app_blog_home">
                                <article>
                                    <h3 class="tittle">
                                        <a target="_blank">
                                            <span>Informação pagamento recorrente:</span>
                                        </a>
                                    </h3>
                                    <textarea class="mce" name="content_recurrency"><?= $textRecurrecy->alert_text ?></textarea>
                                </article>
                            </div>
                    </div>

                    <div class="dash_content_app_box">
                        <section>
                            <div class="app_blog_home">
                                <article>
                                    <h3 class="tittle">
                                        <a target="_blank">
                                            <span>Valores Padrões (Favor separa sempre por -):</span>
                                        </a>
                                    </h3>
                                    <textarea class="mce" name="content_pags" id="content_pags"> <?= $textPag->pag_text ?></textarea>
                                </article>
                            </div>
                    </div>
                    <div class="padding">
                        <div class="control-group">
                            <div class="control-group">
                                <div class="form-group btn-custom"><button class="btn btn-success" type="submit">Salvar</button></div>
                            </div>
                        </div>
                    </div>
                </section>   
            </form>
        </section>
    </div>
    <script src="assets/shared/scripts/jquery.min.js"></script>
    <script src="assets/shared/scripts/jquery.form.js"></script>
    <script src="assets/shared/scripts/jquery-ui.js"></script>
    <script src="assets/shared/scripts/jquery.mask.js"></script>
    <script src="assets/shared/scripts/tinymce/tinymce.min.js"></script>
    <script src="assets/admin/assets/js/scripts.js"></script>
</body>