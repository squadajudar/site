<?php require 'config.php'; ?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Bem Vindo(a) Administrador | CTA </title> 
    <link rel="stylesheet" href="assets/shared/styles/boot.css">
    <link rel="stylesheet" href="assets/shared/styles/styles.css">
    <link rel="stylesheet" href="assets/admin/assets/css/style.css">
    <link rel="stylesheet" href="assets/admin/assets/css/login.css">

    <link rel="shortcut icon" href="http://gabrielhenriq.com.br/efs/wp-content/themes/esfbrasil/assets/img/favicon.png">


    <link rel="stylesheet" type="text/css" href="assets/plugin/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugin/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugin/css/component.css" />

</head>
<body>
    <div class="ajax_response"></div>
    <div class="dash">
        <aside class="dash_sidebar">
            <article class="dash_sidebar_user">
                <div><img class="dash_sidebar_user_thumb" src="http://gabrielhenriq.com.br/efs/wp-content/themes/esfbrasil/assets/img/favicon.png" alt="" title=""></div>
                <h3 class="dash_sidebar_user_name">
                    <a href="<?= BASE ?>/read">Administrador</a>
                </h3>
            </article>
        </aside>

        <section class="dash_content">
            <div class="dash_userbar">
                <div class="dash_userbar_box">
                    <div class="dash_content_box">
                        <h1 class="transition"> 
                            <a href="<?= BASE ?>read">CMS <b>Admin</b></a></h1>
                    </div>
                </div>
                <div class="notification_center"></div>
            </div>
            <?php
            $ip = new App\Access();

            if (!empty($_POST)) {
                include 'process-page/send.php';
            }

            if (!$ip->checkIp($_SERVER["REMOTE_ADDR"])) {

                include 'assets/scrips.php';

                if (empty($_POST)) {
                    echo "<script>Swal.fire({ 
        icon: 'error',
        title: 'Oops...',
        text: 'Acesso restrito, solicite seu acesso no formulário abaixo!'
    })</script>";
                } else {
                    echo "<script>Swal.fire({ 
        icon: 'success',
        title: 'Acesso Solicitado...',
        text: 'Acesso solicitado, agora basta aguardar para os administradores validarem seu acesso!'
    })</script>";
                }

                include 'process-page/form.php';
                die;
            }
            include 'process-page/load-donates.php';
            ?>
        </section>
    </div>
</body>